# MathCalulator #

This simple calculator is built for university classes purposes and use ANTLR library.

### How to execute it for the first time? ###

gradlew antlr run

### How to execute it after? ###

gradlew run

### Configuration ###

gradle's _antlr_ task rebuilds generated code

### Author ###

* Mateusz Bednarski 
* Mateusz Dudek
