grammar Calculator;

eval
	: numericExpression
	| matrixExpression
	;

matrixSimple
	: matrix
	| '(' matrixExpression ')'
	| matrixTransposition
	;

numericSimple
	: number
	| '(' numericExpression ')'
	| matrixDeterminant
	;

matrixExpression
	: matrixSimple
	| numericSimple MultiplySign matrixExpression
	| matrixExpression (MultiplySign | HadamardSign) matrixExpression
	| matrixExpression AdditionSign matrixExpression
	;

numericExpression
	: numericSimple
	| numericExpression (MultiplySign | DivideSign) numericExpression
	| numericExpression (AdditionSign | SubtractionSign) numericExpression
	;

MultiplySign
	: '*'
	;

DivideSign
	: '/'
	;

AdditionSign
	: '+'
	;

SubtractionSign
	: '-'
	;

HadamardSign
	: 'o'
	;

matrixDeterminant
	: '|' matrixExpression '|'
	;

matrixTransposition
	: matrix 'T'
	;

Int
	: ('-')? [0-9]+
	;

Decimal
	: ('-')? [0-9]+ '.' [0.9]+
	;

number
	: Int
	| Decimal
	;

matrix
	: '[' rows? ']'
	;

rows
	: row (';' row )* (';')?
	;

row
	: number (',' number)* (',')?
	;


WS
	: ( ' ' | '\t' | '\r' | '\n')+ -> skip
	;
