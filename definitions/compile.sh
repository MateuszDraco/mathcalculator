#!/bin/bash

# arguments:
# 1 - antlr jar file
# 2 - definion name
# 3 - output directory
# 4 - package

antlr=$1
src=$2
out=${3:-'src/main/java'}
pckg=${4:-'com.mdraco'}

if [[ -z $src || ! -e $src ]]; then
  echo "Source file does not exists: $src"
elif [ ! -d $out ]; then
  echo "Destination dictionary does not exists: $out"
elif [[ -z $antlr || ! -e $antlr ]]; then
  echo "Missing antlr library"
else
  echo "Compiling $src to $out with package $pckg. [Library: $antlr]"

  pckdir=`echo "$pckg" | tr '.' '/'`

  java -jar $antlr -o "$out/$pckdir" -package $pckg $src
fi
