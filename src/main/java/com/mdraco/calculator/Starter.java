package com.mdraco.calculator;

import com.mdraco.calculator.compiler.Calculator;
import com.mdraco.calculator.model.Value;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class Starter {

    Calculator calculator = new Calculator();

    public static void main(String[] args) {
        Starter starter = new Starter();
        starter.showSamples();
        starter.calculateInput();
    }

    private void showSamples() {
        System.out.println("Some calculator samples: ");
        calculate("1+ 2* 3+4");
        calculate("2+3*4");
        calculate("2*[1,2;3,4]+[19, 17; 15, 13]");
        Value result = calculate("[0, 1, 2, 7; 1, 2, 3, 4; 5, 6, 7, 8; -1, 1, -1, 1]");
        calculate("|" + result + "|");
        calculate("[1, 0, 2; -1, 3, 1]*[3, 1; 2, 1; 1, 0]");
        calculate("[ 1,  2,  3;  5,  8, 13] o [11,  9,  7;  5,  3,  1]");
    }

    private Value calculate(String input) {
        System.out.print(input);
        System.out.print(" ===> ");
        Value value = calculator.evaluate(input);
        System.out.println(value.toString());
        return value;
    }

    private void calculateInput() {
        System.out.println("Empty line == quit");
        String input = readLine();
        while (input != null && input.length() > 0) {
            System.out.println(calculator.evaluate(input));
            input = readLine();
        }
        System.out.println("BYE!");
    }

    private String readLine() {
        try {
            return new BufferedReader(new InputStreamReader(System.in)).readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }
}
