package com.mdraco.calculator.compiler;

import com.mdraco.calculator.model.*;
import com.mdraco.calculator.parser.CalculatorLexer;
import com.mdraco.calculator.parser.CalculatorParser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;

import java.util.Collection;
import java.util.List;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;

/**
 * User: mateusz
 * Date: 06.01.2015
 * Time: 21:25
 * Created with IntelliJ IDEA.
 */
public class Calculator {

	private CalculatorParser prepareParser(String input) {
		CharStream charStream = new ANTLRInputStream(input);
		CalculatorLexer lexer = new CalculatorLexer(charStream);
		TokenStream stream = new CommonTokenStream(lexer);
		return new CalculatorParser(stream);
	}

	public Value evaluate(String input) {
		CalculatorParser parser = prepareParser(input);
		CalculatorParser.EvalContext c = parser.eval();
		return c.numericExpression() != null
				? calculate(c.numericExpression())
				: calculate(c.matrixExpression());
	}

	private NumericValue calculate(CalculatorParser.NumericExpressionContext numericExpressionContext) {
		if (numericExpressionContext.numericSimple() != null)
			return calculate(numericExpressionContext.numericSimple());
		if (numericExpressionContext.MultiplySign() != null)
			return calculate(numericExpressionContext.numericExpression(), NumericValue::multiply);
		if (numericExpressionContext.DivideSign() != null)
			return calculate(numericExpressionContext.numericExpression(), NumericValue::divide);
		if (numericExpressionContext.AdditionSign() != null)
			return calculate(numericExpressionContext.numericExpression(), NumericValue::add);
		if (numericExpressionContext.SubtractionSign() != null)
			return calculate(numericExpressionContext.numericExpression(), NumericValue::subtract);
		throw new UnsupportedOperationException("missing NumericExpression handler");
	}

	private NumericValue calculate(List<CalculatorParser.NumericExpressionContext> expressions, BinaryOperator<NumericValue> operation) {
		return expressions
				.stream()
				.map(this::calculate)
				.reduce(operation)
				.orElse(IntValue.Zero);
	}

	private NumericValue calculate(CalculatorParser.NumericSimpleContext numericSimpleContext) {
		if (numericSimpleContext.number() != null)
			return calculate(numericSimpleContext.number());
		if (numericSimpleContext.numericExpression() != null)
			return calculate(numericSimpleContext.numericExpression());
		if (numericSimpleContext.matrixDeterminant() != null)
			return calculate(numericSimpleContext.matrixDeterminant().matrixExpression()).getDeterminant();

		throw new UnsupportedOperationException("missing NumericSimple handler");
	}

	private NumericValue calculate(CalculatorParser.NumberContext number) {
		if (number.Int() != null)
			return new IntValue(Integer.parseInt(number.getText()));
		if (number.Decimal() != null)
			return new DecimalValue(Double.parseDouble(number.getText()));
		throw new UnsupportedOperationException("missing Number handler");
	}

	private MatrixValue calculate(CalculatorParser.MatrixExpressionContext matrixExpressionContext) {
		if (matrixExpressionContext.AdditionSign() != null) {
			return matrixExpressionContext
					.matrixExpression()
					.stream()
					.map(this::calculate)
					.reduce(MatrixValue::add)
					.get();
		}
		if (matrixExpressionContext.MultiplySign() != null) {
			if (matrixExpressionContext.numericSimple() != null) {
				MatrixValue matrix = calculate(matrixExpressionContext.matrixExpression(0));
				NumericValue numeric = calculate(matrixExpressionContext.numericSimple());

				return matrix.multiply(numeric);
			}
			MatrixValue matrixA = calculate(matrixExpressionContext.matrixExpression(0));
			MatrixValue matrixB = calculate(matrixExpressionContext.matrixExpression(1));

			return matrixA.multiply(matrixB);
		}
		if (matrixExpressionContext.HadamardSign() != null) {
			MatrixValue matrixA = calculate(matrixExpressionContext.matrixExpression(0));
			MatrixValue matrixB = calculate(matrixExpressionContext.matrixExpression(1));

			return matrixA.hadamardProduct(matrixB);
		}
		if (matrixExpressionContext.matrixSimple() != null)
			return calculate(matrixExpressionContext.matrixSimple());

		throw new UnsupportedOperationException("missing MatrixExpression handler");
	}

	private MatrixValue calculate(CalculatorParser.MatrixSimpleContext matrixSimpleContext) {
		if (matrixSimpleContext.matrix() != null)
			return calculate(matrixSimpleContext.matrix());
		if (matrixSimpleContext.matrixExpression() != null)
			return calculate(matrixSimpleContext.matrixExpression());
		if (matrixSimpleContext.matrixTransposition() != null)
			return calculate(matrixSimpleContext.matrixTransposition().matrix()).transposition();

		throw new UnsupportedOperationException("missing MatrixSimple handler");
	}

	private MatrixValue calculate(CalculatorParser.MatrixContext matrix) {
		List<List<NumericValue>> rows = matrix
				.rows()
				.row()
				.stream()
				.map(this::calculate)
				.collect(Collectors.toList());
		boolean allTheSameSize = !rows
				.stream()
				.skip(1)
				.anyMatch(r -> r.size() != rows.get(0).size());
		if (!allTheSameSize)
			throw new UnsupportedOperationException("all rows should have the same size");

		int numberOfCols = rows
				.stream()
				.findFirst()
				.map(List::size)
				.orElseGet(() -> 0);
		return new MatrixValue(numberOfCols, rows.stream().flatMap(Collection::stream).collect(Collectors.toList()));
	}

	private List<NumericValue> calculate(CalculatorParser.RowContext rowContext) {
		return rowContext
			.number()
			.stream()
			.map(this::calculate)
			.collect(Collectors.toList());
	}
}
/*
	private Value calculate(CalculatorParser.ExpressionContext expression) {
		if (expression.simple() != null) {
			return calculate(expression.simple());
		}
		if (expression.multiply() != null) {
			return calculate(expression.multiply());
		}
		if (expression.addition() != null) {
			return calculate(expression.addition());
		}
		if (expression.subtraction() != null)
			return calculate(expression.subtraction());
		return null;
	}

	private Value calculate(CalculatorParser.SubtractionContext subtraction) {
		Value left = subtraction.simple() != null
				? calculate(subtraction.simple())
				: subtraction.multiply() != null
				? calculate(subtraction.multiply())
				: calculate(subtraction.dividing());
		return calculate(new Minus(), left, subtraction.expression());
	}

	private Value calculate(CalculatorParser.AdditionContext addition) {
		Value left = addition.simple() != null
				? calculate(addition.simple())
				: addition.multiply() != null
					? calculate(addition.multiply())
					: calculate(addition.dividing());
		return calculate(new Addition(), left, addition.expression());
	}

	private Value calculate(CalculatorParser.DividingContext dividing) {
		Operation operation = new Multiplication();
		operation.add(calculate(dividing.simple(0)));
		operation.add(calculate(dividing.simple(1)));
		return operation.evaluate();
	}

	private Value calculate(Operation operation, Value leftArgument, List<CalculatorParser.ExpressionContext> restOfArguments) {
		operation.add(leftArgument);
		restOfArguments
				.stream()
				.forEach(e -> operation.add(calculate(e)));
		return operation.evaluate();
	}

	private Value calculate(CalculatorParser.MultiplyContext multiply) {
		Operation operation = new Multiplication();
		multiply.simple()
				.stream()
				.forEach(s -> operation.add(calculate(s)));
		return operation.evaluate();
	}

	private Value calculate(CalculatorParser.SimpleContext simple) {
		if (simple.value() != null)
			return calculate(simple.value());
		if (simple.expression() != null)
			return calculate(simple.expression());
		if (simple.determinant() != null) {
			MatrixValue matrix = calculate(simple.determinant().matrix());
			return new IntValue(matrix.getDeterminant());
		}

		throw new UnsupportedOperationException("Cannot evaluate SIMPLE expression");
	}

	private Value calculate(CalculatorParser.ValueContext value) {
		return value.Number() != null
				? calculate(value.Number())
				: calculate(value.matrix());
	}

	private IntValue calculate(TerminalNode number) {
		return new IntValue(Integer.parseInt(number.getText()));
	}

	private MatrixValue calculate(CalculatorParser.MatrixContext matrix) {
		List<List<IntValue>> values = matrix
				.rows()
				.row()
				.stream()
				.map(row -> row
						.Number()
						.stream()
						.map(this::calculate)
						.collect(Collectors.toList()))
				.collect(Collectors.toList());
		int columns = (values.size() > 0)
				? values.get(0).size()
				: 0;

		if (columns <= 0 || values.stream().anyMatch(v -> v.size() != columns))
			throw new UnsupportedOperationException("invalid matrix definition");

		return new MatrixValue(columns, values.stream().flatMap(v -> v.stream().map(Value::getValue)).collect(Collectors.toList()));
	}

	private CalculatorParser prepareParser(String input) {
		CharStream charStream = new ANTLRInputStream(input);
		CalculatorLexer lexer = new CalculatorLexer(charStream);
		TokenStream stream = new CommonTokenStream(lexer);
		return new CalculatorParser(stream);
	}
}
*/