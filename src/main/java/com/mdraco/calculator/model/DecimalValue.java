package com.mdraco.calculator.model;

/**
 * User: mateusz
 * Date: 10.01.2015
 * Time: 20:10
 * Created with IntelliJ IDEA.
 */
public class DecimalValue implements NumericValue {

	private double value;

	public DecimalValue(double value) {
		this.value = value;
	}

	public double getValue() {
		return value;
	}

	@Override
	public NumericValue multiply(NumericValue value) {
		if (value instanceof IntValue)
			return new DecimalValue(this.value * ((IntValue)value).getValue());
		return new DecimalValue(this.value * ((DecimalValue) value).getValue());
	}

	@Override
	public NumericValue add(NumericValue value) {
		if (value instanceof IntValue)
			return new DecimalValue(this.value + ((IntValue) value).getValue());
		return new DecimalValue(this.value + ((DecimalValue) value).getValue());
	}

	@Override
	public NumericValue divide(NumericValue value) {
		if (value instanceof IntValue)
			return new DecimalValue(this.value / ((IntValue) value).getValue());
		return new DecimalValue(this.value / ((DecimalValue) value).getValue());
	}

	@Override
	public NumericValue subtract(NumericValue value) {
		if (value instanceof IntValue)
			return new DecimalValue(this.value - ((IntValue) value).getValue());
		return new DecimalValue(this.value - ((DecimalValue) value).getValue());
	}

	@Override
	public String toString() {
		return Double.toString(value);
	}
}
