package com.mdraco.calculator.model;

/**
 * User: mateusz
 * Date: 06.01.2015
 * Time: 13:55
 * Created with IntelliJ IDEA.
 */
public class IntValue implements NumericValue {
	public static NumericValue Zero = new IntValue(0);
	public static NumericValue MinusOne = new IntValue(-1);

	private final int value;

	public IntValue(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	@Override
	public NumericValue add(NumericValue value) {
		if (value instanceof DecimalValue)
			return add((DecimalValue) value);
		return add((IntValue) value);
	}

	@Override
	public NumericValue divide(NumericValue value) {
		double right = value instanceof DecimalValue
				? ((DecimalValue)value).getValue()
				: (double)((IntValue)value).getValue();
		return new DecimalValue(this.value / right);
	}

	@Override
	public NumericValue subtract(NumericValue value) {
		if (value instanceof DecimalValue)
			return subtract((DecimalValue) value);
		return subtract((IntValue) value);
	}

	public IntValue add(IntValue value) {
		return new IntValue(getValue() + value.getValue());
	}

	public DecimalValue add(DecimalValue value) {
		return new DecimalValue(getValue() + value.getValue());
	}

	public IntValue multiply(IntValue value) {
		return new IntValue(getValue() * value.getValue());
	}

	public DecimalValue multiply(DecimalValue value) {
		return new DecimalValue(getValue() * value.getValue());
	}

	public IntValue subtract(IntValue value) {
		return new IntValue(getValue() - value.getValue());
	}

	public DecimalValue subtract(DecimalValue value) {
		return new DecimalValue(getValue() - value.getValue());
	}

	@Override
	public NumericValue multiply(NumericValue value) {
		if (value instanceof DecimalValue)
			return multiply((DecimalValue)value);
		return multiply((IntValue)value);
	}

	public MatrixValue multiply(MatrixValue value) {
		MatrixValue matrix = new MatrixValue(value.getColumnCount(), value.getRowCount());

		for (int row = 1; row <= value.getRowCount(); ++row) {
			for (int col = 1; col <= value.getColumnCount(); ++col) {
				matrix.set(col, row, multiply(value.get(col, row)));
			}
		}
		return matrix;
	}

	@Override
	public String toString() {
		return Integer.toString(value);
	}
}
