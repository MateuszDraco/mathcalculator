package com.mdraco.calculator.model;

import java.util.*;
import java.util.stream.Collectors;

/**
 * User: mateusz
 * Date: 06.01.2015
 * Time: 13:56
 * Created with IntelliJ IDEA.
 */
public class MatrixValue implements Value {
	private final int columnCount;
	private final List<NumericValue> values;

	public MatrixValue(int columnCount, List<NumericValue> values) {
		this.columnCount = columnCount;
		this.values = values;
	}

	public MatrixValue(int columnCount, NumericValue... values) {
		this(columnCount, Arrays.asList(values));
	}

	public MatrixValue(int columnCount, int rowCount) {
		this(columnCount, Arrays.asList(new NumericValue[columnCount * rowCount]));
	}

	public void set(int row, int col, NumericValue value) {
		values.set(getPosition(row, col), value);
	}

	public NumericValue get(int row, int col) {
		return values.get(getPosition(row, col));
	}

	private int getPosition(int row, int col) {
		int pos = columnCount * (row - 1) + (col - 1);
		if (pos >= values.size())
			throw new UnsupportedOperationException("matrix is not big enough: " + pos + " at (" + row + ", " + col + ") size=" + values.size() + "(" + getRowCount() + ", " + getColumnCount()  + ")");
		if (pos < 0)
			throw new UnsupportedOperationException("given arguments are invalid: " + row + ", " + col);
		return pos;
	}

	public NumericValue getDeterminant() {
		if (columnCount * columnCount == values.size()) {
			if (values.size() == 1)
				return values.get(0);
			NumericValue sum = IntValue.Zero;
			for (int c = 1; c <= columnCount; ++c) {
				NumericValue subtotal = cutOff(1, c).getDeterminant().multiply(get(1, c));
				if ((c % 2) == 0)
					subtotal = subtotal.multiply(IntValue.MinusOne);
				sum = sum.add(subtotal);
			}
			return sum;
		}
		return IntValue.Zero;
	}

	private MatrixValue cutOff(int row, int col) {
		ArrayList<NumericValue> list = new ArrayList<>((getColumnCount() - 1) * (getRowCount() - 1));
		for (int b = 1; b <= getRowCount(); ++b) {
			if (b != row) {
				for (int a = 1; a <= getColumnCount(); ++a) {
					if (a != col)
						list.add(get(b, a));
				}
			}
		}

		return new MatrixValue(getColumnCount() - 1, list);
	}

	public int getColumnCount() {
		return columnCount;
	}

	public int getRowCount() {
		return values.size() / columnCount;
	}

	public MatrixValue add(MatrixValue second) {
		if (getColumnCount() != second.getColumnCount()
				|| getRowCount() != second.getRowCount())
			throw new UnsupportedOperationException("cannot add different size matrix's");

		MatrixValue matrix = CopyZero();
		for (int row = 1; row <= getRowCount(); ++row) {
			for (int col = 1; col <= getColumnCount(); ++col)
				matrix.set(row, col, get(row, col).add(second.get(row, col)));
		}
		return matrix;
	}

	public MatrixValue multiply(NumericValue scalar) {
		return new MatrixValue(getColumnCount(), values.stream().map(v -> v.multiply(scalar)).collect(Collectors.toList()));
	}

	public MatrixValue CopyZero() {
		return new MatrixValue(getColumnCount(), getRowCount());
	}

	@Override
	public String toString() {
		StringBuilder bob = new StringBuilder();
		Formatter formatter = new Formatter(bob, Locale.US);
		formatter.format("[%n");
		for (int y = 1; y <= getRowCount(); ++y) {
			bob.append("\t");
			for (int x = 1; x <= getColumnCount(); ++x) {
				formatter.format("%1$3s", get(y, x).toString());
				bob.append(x != getColumnCount() ? ", " : ";");
			}
			formatter.format("%n");
		}
		bob.append("]");
		return bob.toString();
	}

	public MatrixValue transposition() {
		MatrixValue matrix = new MatrixValue(getRowCount(), getColumnCount());

		for (int row = 1; row <= getRowCount(); ++row) {
			for (int col = 1; col <= getColumnCount(); ++col)
				matrix.set(col, row, get(row, col));
		}

		return matrix;
	}

	public MatrixValue hadamardProduct(MatrixValue second) {
		if (getColumnCount() != second.getColumnCount()
				|| getRowCount() != second.getRowCount())
			throw new UnsupportedOperationException("incompatible matrix size");

		MatrixValue result = new MatrixValue(getColumnCount(), getRowCount());
		for (int row = 1; row <= getRowCount(); ++row) {
			for (int col = 1; col <= getColumnCount(); ++col)
				result.set(row, col, get(row, col).multiply(second.get(row, col)));
		}
		return result;
	}

	public MatrixValue multiply(MatrixValue second) {
		if (getColumnCount() != second.getRowCount()
				|| getRowCount() != second.getColumnCount())
			throw new UnsupportedOperationException("incompatible matrix size");

		MatrixValue result = new MatrixValue(second.getColumnCount(), getRowCount());
		for (int row = 1; row <= result.getRowCount(); ++row) {
			for (int col = 1; col <= result.getColumnCount(); ++col) {
				NumericValue sum = IntValue.Zero;
				for (int s = 1; s <= getColumnCount(); ++s) {
					NumericValue left = get(row, s);
					NumericValue right = second.get(s, col);
					sum = sum.add(left.multiply(right));
				}
				result.set(row, col, sum);
			}
		}
		return result;
	}
}
