package com.mdraco.calculator.model;

/**
 * User: mateusz
 * Date: 10.01.2015
 * Time: 20:42
 * Created with IntelliJ IDEA.
 */
public interface NumericValue extends Value {
	NumericValue multiply(NumericValue value);
	NumericValue add(NumericValue value);
	NumericValue divide(NumericValue value);
	NumericValue subtract(NumericValue value);
}
