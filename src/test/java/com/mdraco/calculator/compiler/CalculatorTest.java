package com.mdraco.calculator.compiler;

import com.mdraco.calculator.model.IntValue;
import com.mdraco.calculator.model.MatrixValue;
import com.mdraco.calculator.model.Value;
import junit.framework.TestCase;

import java.util.Arrays;
import java.util.stream.Collectors;

public class CalculatorTest extends TestCase {

	private Calculator calculator;

	private MatrixValue createMatrix(int columns, Integer... values) {
		return new MatrixValue(columns, Arrays.asList(values).stream().map(IntValue::new).collect(Collectors.toList()));
	}

	public void setUp() throws Exception {
		super.setUp();
		calculator = new Calculator();
	}

	public void testMath1Evaluate() throws Exception {
		Value value = calculator.evaluate("1+ 2* 3+4");
		assertEquals("11", value.toString());
	}

	public void testMath2Evaluate() throws Exception {
		Value value = calculator.evaluate("[0, 1, 2, 7; 1, 2, 3, 4; 5, 6, 7, 8; -1, 1, -1, 1]");
		Value expected = createMatrix(4, 0, 1, 2, 7, 1, 2, 3, 4, 5, 6, 7, 8, -1, 1, -1, 1);
		assertEquals(expected.toString(), value.toString());
	}

	public void testDeterminant() throws Exception {
		Value value = calculator.evaluate("|[0, 1, 2, 7; 1, 2, 3, 4; 5, 6, 7, 8; -1, 1, -1, 1]|");
		assertEquals("-64", value.toString());
	}

	public void testMatrixMultiplication() throws Exception {
		Value value = calculator.evaluate("[1, 0, 2; -1, 3, 1]*[3, 1; 2, 1; 1, 0]");
		Value expected = createMatrix(2, 5, 1, 4, 2);
		assertEquals(expected.toString(), value.toString());
	}

	public void testMatrixHadamard() throws Exception {
		Value value = calculator.evaluate("[ 1,  2,  3;  5,  8, 13] o [11,  9,  7;  5,  3,  1]");
		Value expected = createMatrix(3, 11, 18, 21, 25, 24, 13);
		assertEquals(expected.toString(), value.toString());
	}
}