package com.mdraco.calculator.model;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class MatrixValueTest {

	private MatrixValue createMatrix(int columns, Integer... values) {
		return new MatrixValue(columns, Arrays.asList(values).stream().map(IntValue::new).collect(Collectors.toList()));
	}

	@Test
	public void testSet() throws Exception {
		MatrixValue matrix = createMatrix(3, 0, 0, 0, 0, 0, 0, 0, 0, 0);

		IntValue i1 = new IntValue(666);
		IntValue i2 = new IntValue(69);

		matrix.set(1, 1, i1);
		matrix.set(3, 3, i2);

		assertEquals(i1, matrix.get(1, 1));
		assertEquals(i2, matrix.get(3, 3));
	}

	@Test
	public void testGet() throws Exception {
		IntValue i1 = new IntValue(1);
		IntValue i2 = new IntValue(2);
		IntValue i3 = new IntValue(3);
		IntValue i4 = new IntValue(4);


		MatrixValue matrix = new MatrixValue(2, i1, i2, i3, i4);

		assertEquals(i1, matrix.get(1, 1));
		assertEquals(i2, matrix.get(1, 2));
		assertEquals(i3, matrix.get(2, 1));
		assertEquals(i4, matrix.get(2, 2));
	}

	@Test
	public void testGetDeterminant() throws Exception {
		IntValue i1 = new IntValue(69);
		MatrixValue matrix = new MatrixValue(1, i1);

		assertEquals(i1, matrix.getDeterminant());
	}

	@Test
	public void testGetDeterminantCalculated() throws Exception {
		MatrixValue matrix = createMatrix(4, 0, 1, 2, 7, 1, 2, 3, 4, 5, 6, 7, 8, -1, 1, -1, 1);
		NumericValue determinant = matrix.getDeterminant();

		assertTrue(determinant instanceof IntValue);
		assertEquals(-64, ((IntValue)determinant).getValue());
	}

	@Test
	public void testGetColumnCount() throws Exception {
		MatrixValue matrix = createMatrix(4, 0, 1, 2, 7, 1, 2, 3, 4, 5, 6, 7, 8, -1, 1, -1, 1);

		assertEquals(4, matrix.getColumnCount());
	}

	@Test
	public void testGetRowCount() throws Exception {
		MatrixValue matrix = createMatrix(4, 0, 1, 2, 7, 1, 2, 3, 4, 5, 6, 7, 8, -1, 1, -1, 1);

		assertEquals(4, matrix.getRowCount());
	}

	@Test
	public void testSizes() throws Exception {
		MatrixValue matrix = new MatrixValue(4, 6);

		assertEquals(4, matrix.getColumnCount());
		assertEquals(6, matrix.getRowCount());
	}

	@Test
	public void testDifferentSizes() throws Exception {
		MatrixValue matrix = createMatrix(3, 1, 2, 3, 4, 5, 6);

		assertEquals(3, matrix.getColumnCount());
		assertEquals(2, matrix.getRowCount());
	}

	@Test
	public void testTransposition() throws  Exception {
		MatrixValue matrix = createMatrix(3, 1, 2, 3, 4, 5, 6);
		MatrixValue t = matrix.transposition();

		assertEquals(matrix.getColumnCount(), t.getRowCount());
		assertEquals(matrix.getRowCount(), t.getColumnCount());
		assertEquals(matrix.get(1, 1), t.get(1, 1));
		assertEquals(matrix.get(1, 2), t.get(2, 1));
		assertEquals(matrix.get(1, 3), t.get(3, 1));
		assertEquals(matrix.get(2, 1), t.get(1, 2));
		assertEquals(matrix.get(2, 2), t.get(2, 2));
		assertEquals(matrix.get(2, 3), t.get(3, 2));
	}

	@Test
	public void testHadamardProduct() throws Exception {
		MatrixValue a = createMatrix(3,  1,  2,  3,  5,  8, 13);
		MatrixValue b = createMatrix(3, 11,  9,  7,  5,  3,  1);
		MatrixValue e = createMatrix(3, 11, 18, 21, 25, 24, 13);

		// c = a o b
		MatrixValue t = a.hadamardProduct(b);
		assertEquals(e.getColumnCount(), t.getColumnCount());
		assertEquals(e.getRowCount(),    t.getRowCount());
		assertEquals(((IntValue) e.get(1, 1)).getValue(), ((IntValue) t.get(1, 1)).getValue());
		assertEquals(((IntValue) e.get(1, 2)).getValue(), ((IntValue) t.get(1, 2)).getValue());
		assertEquals(((IntValue) e.get(1, 3)).getValue(), ((IntValue) t.get(1, 3)).getValue());
		assertEquals(((IntValue) e.get(2, 1)).getValue(), ((IntValue) t.get(2, 1)).getValue());
		assertEquals(((IntValue) e.get(2, 2)).getValue(), ((IntValue) t.get(2, 2)).getValue());
		assertEquals(((IntValue) e.get(2, 3)).getValue(), ((IntValue) t.get(2, 3)).getValue());
	}

	@Test
	public void testMultiplication() throws Exception {
		MatrixValue a = createMatrix(3, 1, 0, 2, -1, 3, 1);
		MatrixValue b = createMatrix(2, 3, 1, 2, 1, 1, 0);
		MatrixValue e = createMatrix(2, 5, 1, 4, 2);

		MatrixValue t = a.multiply(b);
		assertEquals(e.getColumnCount(), t.getColumnCount());
		assertEquals(e.getRowCount(), t.getRowCount());
		assertEquals(((IntValue) e.get(1, 1)).getValue(), ((IntValue) t.get(1, 1)).getValue());
		assertEquals(((IntValue) e.get(1, 2)).getValue(), ((IntValue) t.get(1, 2)).getValue());
		assertEquals(((IntValue) e.get(2, 1)).getValue(), ((IntValue) t.get(2, 1)).getValue());
		assertEquals(((IntValue) e.get(2, 2)).getValue(), ((IntValue) t.get(2, 2)).getValue());
	}
}
